package com.company;

public interface Student {

    String printInfo(String name, String lastName);
    String getCourse();
    
}
