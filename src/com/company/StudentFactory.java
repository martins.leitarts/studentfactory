package com.company;

public class StudentFactory {

    public Student getStudent(int course){
        Student student;

        switch(course){
            case 1:
                student = new firstYear();
                break;
            case 2:
                student = new middle();
                break;
            case 3:
                student = new Senior();
                break;
            default:
                student = null;
                break;
        }

        return student;
    }
}
