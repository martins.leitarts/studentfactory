package com.company;

public class Main {

    public static void main(String[] args) {
	    //Izmantojot factory design pattern
        //Uztaisit StudentFactory klasi
        //Katram studentam var but vards, uzvards un kurs kurss- noteikt pec klases
        //Uztaisit 3 klases- firstYear, middle un senior

        //Katra kursa klase jabut metodei, kas atgriez to, kura kurss students sis
        //studentu objekts ir


        StudentFactory factory = new StudentFactory();

        Student student1 = factory.getStudent(1);
        System.out.println(student1.getCourse());
        System.out.println(student1.printInfo("Fluffy", "Unicorn"));

        Student student2 = factory.getStudent(2);
        System.out.println(student2.getCourse());

        Student student3 = factory.getStudent(3);
        System.out.println(student3.getCourse());


    }
}
